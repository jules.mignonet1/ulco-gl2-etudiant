#include "Board.hpp"
#include "ReportStdout.hpp"

void testBoard(Board & b) {
    ReportStdout res;
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    res.reportStdout(b);
}

int main() {

    Board b1;
    testBoard(b1);

    return 0;
}