#pragma once

#include "Itemable.hpp"

#include <fstream>
#include <iostream>

class ReportStdout   {
    public:

        void reportStdout(const Itemable& items) {
            for (const std::string & item : items.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        };
};