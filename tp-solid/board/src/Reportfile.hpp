#pragma once

#include "Itemable.hpp"

#include <fstream>
#include <iostream>

class ReportFile {
    private:
        std::ofstream _ofs;
    
    public:
        ReportFile(const std::string & filename) : _ofs(filename) {}
        void reportFile(const Itemable& items) {
            for (const std::string & item : items.getItems())
                _ofs << item << std::endl;
            _ofs << std::endl;
        }
}